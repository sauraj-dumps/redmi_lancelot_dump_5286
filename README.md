## lancelot-user 11 RP1A.200720.011 V12.5.3.0.RJCMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: lancelot
- Brand: Redmi
- Flavor: lineage_lancelot-userdebug
- Release Version: 11
- Id: RQ3A.211001.001
- Incremental: eng.ben.20220116.135945
- Tags: dev-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Redmi/lancelot_global/lancelot:11/RP1A.200720.011/V12.5.3.0.RJCMIXM:user/release-keys
- OTA version: 
- Branch: lancelot-user-11-RP1A.200720.011-V12.5.3.0.RJCMIXM-release-keys
- Repo: redmi_lancelot_dump_5286


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
